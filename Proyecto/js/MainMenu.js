/**
 * Created by Mrguyfawkes on 16/11/2014.
 */
Edutainment.MainMenu = function(game){};
Edutainment.MainMenu.prototype = {
    create: function(){
        // display images
        this.add.sprite(0, 0, 'background');
        //this.add.sprite((Candy.GAME_WIDTH-395)/2, 60, 'title');
        // add the button that will start the game
        this.add.button(260, 280, 'button-start', this.startGame, this, 1, 0, 2);
    },
    startGame: function() {
        // start the Game state
        this.state.start('Game');
    }
};