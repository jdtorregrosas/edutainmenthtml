var Edutainment = {};

Edutainment.Boot = function(){};

//setting game configuration and loading the assets for the loading screen
Edutainment.Boot.prototype = {
    preload: function() {
        this.load.image('preloadBar', 'assets/images/preloader-bar.png');
    },
    create: function() {
        this.game.input.maxPointers = 1;
        this.game.state.start('Preload');
    }
};