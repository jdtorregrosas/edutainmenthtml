Edutainment.Preload = function(game) {};

Edutainment.Preload.prototype = {
    preload: function() {
        //show loading screen
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY + 128, 'preloadBar');
        this.preloadBar.anchor.setTo(0.5);

        this.load.setPreloadSprite(this.preloadBar);

        //load game assets
        this.load.image('background', 'assets/MainMenu.png', 480, 480);
        this.load.spritesheet('button-start', 'assets/Boton.png', 200, 60);

        this.load.spritesheet('dude', 'assets/playerSpread.png', 28, 28);
        this.load.spritesheet('box', 'assets/box.png', 32, 32);
        this.load.tilemap('level1', 'assets/mapaInicio.json', null, Phaser.Tilemap.TILED_JSON);
        this.load.image('tiles', 'assets/tiles.png', 32, 32);

    },
    create: function() {
        this.game.state.start('MainMenu');
    }
};