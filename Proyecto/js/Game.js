Edutainment.Game = function(game) {
    score=0;
    scoreText=null;
    map=null;
    layer=null;
    player=null;
    cursors=null;
    boxes=null;
    box1=null;
    box2=null;
    box3=null;
};
Edutainment.Game.prototype = {
create:function() {
    //  A Tilemap object just holds the data needed to describe the map (i.e. the json exported from Tiled, or the CSV exported from elsewhere).
    //  You can add your own data or manipulate the data (swap tiles around, etc) but in order to display it you need to create a TilemapLayer.
    this.game.physics.startSystem(Phaser.Physics.ARCADE);


    map = this.game.add.tilemap('level1');

    map.addTilesetImage('tiles');
    map.setCollision([1,11,21]);

    layer = map.createLayer(0);



    layer.resizeWorld();



    boxes = this.game.add.group();

    boxes.enableBody = true;


    //  Here we'll create 12 of them evenly spaced apart
    box1 = boxes.create(2 * 32, 3*32, 'box');
    box2 = boxes.create(4 * 32, 8*32, 'box');
    box3 = boxes.create(13 * 32, 10*32, 'box');


    // The player and its settings
    player = this.game.add.sprite(32, this.game.world.height - 150, 'dude');

    //  We need to enable physics on the player
    this.game.physics.arcade.enable(player);

    //player.body.allowGravity=false;
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('left', [2, 6, 10], 10, true);
    player.animations.add('right', [3, 7, 11], 10, true);
    //  Walking up and down
    player.animations.add('up', [1, 5, 9], 10, true);
    player.animations.add('down', [0, 4, 8], 10, true);


    scoreText = this.game.add.text(16, 16, 'Score:'+score, { fontSize: '32px', fill: '#000' });

    //player.body.tilePadding.set(32, 32);
    this.game.camera.follow(player);
    cursors = this.game.input.keyboard.createCursorKeys();
},

update:function(){
    //  Collide the player and the stars with the platforms
    this.game.physics.arcade.collide(player, layer);

    this.game.physics.arcade.overlap(player, boxes, this.collectBox, null, this);
    this.game.physics.arcade.overlap(player, boxes, this.collectBox, null, this);

    cursors = this.game.input.keyboard.createCursorKeys();

    //  Reset the players velocity (movement)

    player.body.velocity.x = 0;
    player.body.velocity.y = 0;

    if (cursors.left.isDown)
    {
        //  Move to the left
        player.body.velocity.x = -150;

        player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        //  Move to the right
        player.body.velocity.x = 150;

        player.animations.play('right');
    }
    else if (cursors.up.isDown)
    {
        //  Stand still
        player.body.velocity.y = -150;

        player.animations.play('up');
    }
    else if (cursors.down.isDown)
    {
        //  Stand still
        player.body.velocity.y = 150;

        player.animations.play('down');
    }
    else{
        player.animations.stop();

    }


    //  Allow the player to jump if they are touching the ground.
    if (cursors.up.isDown && (player.body.onFloor() || player.body.touching.down))
    {
        player.body.velocity.y = -300;
    }
},
collectBox:function (player, box) {

    // Removes the star from the screen

    box.animations.add('close', [1], 10, false);
    box.animations.play('close');

    //  Add and update the score
    box.alpha=0.5;
    score += 150;
    scoreText.text = 'Score: ' + score;
    boxes.enableBody = false;

}
};